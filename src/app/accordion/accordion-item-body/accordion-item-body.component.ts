import {Component, OnInit, ElementRef, Output, EventEmitter, Renderer, ViewChild} from '@angular/core';

@Component({
  exportAs: 'accordionItemBody',
  selector: 'accordion-item-body',
  templateUrl: './accordion-item-body.component.html',
  styleUrls: ['./accordion-item-body.component.scss']
})
export class AccordionItemBodyComponent implements OnInit {

  @ViewChild('body') bodyEl: ElementRef;
  //@Output() next: EventEmitter<any> = new EventEmitter<any>();

  constructor(private renderer: Renderer) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    const el = this.bodyEl.nativeElement;
    //el.addEventListener('transitionend', (e:TransitionEvent) => { 
        // check transition ended, so can use regular height if not expanded
        //if (el.offsetHeight !== 0) {
            this.setHeight('auto');
       // }
   // }, false);
  }

  toggle(collapsed: boolean) {
      let height: string = '0';
      const el = this.bodyEl.nativeElement;
      if (!collapsed) {
          height = '100%';
      } else {
          height = '0';
      }
      this.setHeight(height)
      //setTimeout(() => this.setHeight(height), 50);
  }

  setHeight(height: string) {
      const el = this.bodyEl.nativeElement;
      this.renderer.setElementStyle(el, 'height', height);
  }

}
