import { Component, OnInit, Input, ContentChild, Inject, forwardRef } from '@angular/core';
import { AccordionItemBodyComponent } from "../accordion-item-body/accordion-item-body.component";
import { AccordionItemHeadComponent } from '../accordion-item-head/accordion-item-head.component';
import { AccordionComponent } from '../accordion.component';

@Component({
  exportAs: 'accordionItem',
  selector: 'accordion-item',
  templateUrl: './accordion-item.component.html',
  styleUrls: ['./accordion-item.component.scss']
})
export class AccordionItemComponent implements OnInit {
  @ContentChild(AccordionItemBodyComponent) body: AccordionItemBodyComponent;
  @ContentChild(AccordionItemHeadComponent) head: AccordionItemBodyComponent;
  @Input() disabled: boolean = false;
  @Input() public collapsed: boolean = true;

  public accordion: AccordionComponent;

  constructor(@Inject(forwardRef(() => AccordionComponent)) accordion: AccordionComponent) {
    this.accordion = accordion;
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.refreshBody();
  }

  refreshBody() {
    this.body.toggle(this.collapsed);
  }

  onToggle(isCollapsed) {
    if(this.disabled) return;
    this.accordion.onItemToggled(this);
    this.collapsed = !isCollapsed;
    this.body.toggle(this.collapsed)
  }

  toggleDisable(bol :boolean) {
    this.disabled =  bol;
  }

}
