import { Component, OnInit, ContentChildren, Output, EventEmitter, forwardRef, QueryList } from '@angular/core';
import { AccordionItemComponent } from "./accordion-item/accordion-item.component";

@Component({
    exportAs: 'accordion',
    selector: 'accordion',
    templateUrl: './accordion.component.html',
    styleUrls: ['./accordion.component.scss']
})
export class AccordionComponent implements OnInit {
    @ContentChildren(forwardRef(() => AccordionItemComponent)) items: QueryList<AccordionItemComponent>;
    @Output() onToggled = new EventEmitter();

    constructor() { }

    ngOnInit() { }

    onItemToggled(item: AccordionItemComponent) {
        if (item.disabled) return;

        if (item.collapsed) {
            this.items.toArray().forEach(function (i) {
                if (i !== item) {
                    if (!i.collapsed) {
                        i.collapsed = true;
                    }
                    i.body.toggle(i.collapsed);
                }
            });
        }

        this.onToggled.emit(this.items.toArray().indexOf(item));
    }
}

