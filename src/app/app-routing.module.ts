import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { GeneralComponent } from './pages/home/form-component/form-step/general/general.component';
import { PersonalComponent } from './pages/home/form-component/form-step/personal/personal.component';
import { TermsComponent } from './pages/home/form-component/form-step/terms/terms.component';

const routes: Routes = [
  { path: '',  redirectTo: '/home/general', pathMatch: 'full' },
  { path: 'home',  redirectTo: '/home/general', pathMatch: 'full' },
  { path: 'home', component: HomeComponent, 
      children:[
        {
          path : 'general',
          component: GeneralComponent,
        },
        {
          path : 'personal',
          component: PersonalComponent,
        },
        {
          path : 'terms',
          component: TermsComponent,
        }
      ] 
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }

