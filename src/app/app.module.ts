import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AccordionComponent } from './accordion/accordion.component';
import { AccordionItemComponent } from './accordion/accordion-item/accordion-item.component';
import { AccordionItemBodyComponent } from './accordion/accordion-item-body/accordion-item-body.component';
import { AccordionItemHeadComponent } from './accordion/accordion-item-head/accordion-item-head.component';

import { FormComponent } from './pages/home/form-component/form-component.component';

import { HomeComponent } from './pages/home/home.component';

import { PersonalComponent } from './pages/home/form-component/form-step/personal/personal.component';
import { TermsComponent } from './pages/home/form-component/form-step/terms/terms.component';
import { NavbarComponent } from './pages/home/form-component/navbar/navbar.component';
import { GeneralComponent } from './pages/home/form-component/form-step/general/general.component';
import { NavbarItemComponent } from './pages/home/form-component/navbar/navbar-item/navbar-item.component';

@NgModule({
  declarations: [
    AppComponent,
    AccordionComponent,
    AccordionItemComponent,
    AccordionItemBodyComponent,
    AccordionItemHeadComponent,
    GeneralComponent,
    PersonalComponent,
    TermsComponent,
    HomeComponent,
    NavbarComponent,
    FormComponent,
    NavbarItemComponent
  ],
  imports: [
    NgbModule.forRoot(),
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
  ],
  providers: [/*FormDataService*/],
  bootstrap: [AppComponent]
})
export class AppModule { }
