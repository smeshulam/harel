import { Component, OnInit, ViewChild } from '@angular/core';
import { AccordionComponent } from '../../../accordion/accordion.component';
import { AccordionItemComponent } from '../../../accordion/accordion-item/accordion-item.component';
import { NavbarComponent } from './navbar/navbar.component';

@Component({
  selector: 'form-component',
  templateUrl: './form-component.component.html',
  styleUrls: ['./form-component.component.scss']
})
export class FormComponent implements OnInit {
  @ViewChild('accordion') accordion: AccordionComponent;

  constructor() { }

  ngOnInit() { }
}
