import { Component, OnInit, Output, EventEmitter, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';

import { General } from '../../formdata.model';
import { FormDataService } from '../../formdata.service';
import $ from 'jquery'
declare var $: $

@Component({
    selector: 'general',
    templateUrl: './general.component.html'
})

export class GeneralComponent implements OnInit {
    @Output() submit = new EventEmitter();
    title = 'בחירת מסלול השקעה';
    formTitle = "באפשרותך לבחור את המסלול שבו יושקע הכסף שלך, על פי צרכיך ומידת הסיכון המתאימה לך."
    general: General;
    form: any;


    constructor(private router: Router, private formDataService: FormDataService) {
    }

    ngOnInit() {
        this.general = this.formDataService.getGeneral();
    }

    ngAfterViewInit() {
        $('select').selectpicker();
    }  

    save(form: any): boolean {
        if (!form.valid) {
            return false;
        }

        this.formDataService.setGeneral(this.general);
        return true;
    }

    goToNext(form: any) {
        if(this.save(form)) {
            this.router.navigate(['/personal']);
            this.submit.emit(event);
        }
    }
}