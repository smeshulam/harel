import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Personal } from '../../formdata.model';
import { FormDataService } from '../../formdata.service';


@Component({
    selector: 'personal',
    templateUrl: './personal.component.html',
})

export class PersonalComponent implements OnInit {
    @Output() submit: EventEmitter<any> = new EventEmitter<any>();
    title = 'בחירת מסלול השקעה';
    personal: Personal;
    form: any;

    constructor(private formDataService: FormDataService) {
    }

    ngOnInit() {
        this.personal = this.formDataService.getPersonal();
    }

    save(form: any): boolean {
        if (!form.valid) {
            return false;
        }

        this.formDataService.setPersonal(this.personal);
        return true;
    }

    goToNext(form: any) {
        if (this.save(form)) {
            //this.router.navigate(['/personal']);
        }
    }
}