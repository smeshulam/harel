import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Terms } from '../../formdata.model';
import { FormDataService } from '../../formdata.service';

@Component({
    selector: 'terms'
    , templateUrl: './terms.component.html'
})

export class TermsComponent implements OnInit {
    title = 'הזנת פרטים אישיים';
    terms: Terms;
    form: any;

    constructor(private router: Router, private formDataService: FormDataService) {
    }

    ngOnInit() {
        this.terms = this.formDataService.getTerms();
    }

    save(form: any): boolean {
        if (!form.valid) {
            return false;
        }

        this.formDataService.setTerms(this.terms);
        return true;
    }

    goToPrevious(form: any) {
        if (this.save(form)) {
            this.router.navigate(['/general']);
        }
    }

    submit() {
        alert('form submit action');
    }

}