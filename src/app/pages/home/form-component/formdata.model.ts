export class FormData {
    deal: number = 1;
    firstName : string = '';
    lastName: string = '';
    address: string = '';
    zip: string = '';
    email: string = '';
    agree: boolean = false;

    clear() {
        this.deal = 1;
        this.firstName = '';
        this.lastName = '';
        this.address = '';
        this.zip = '';
        this.email = '';
        this.agree = false;
    }
}

export const STEPS = {
    general: 'general',
    personal: 'personal',
    terms: 'terms',
}

export class General {
    deal: number = 1;
    firstName: string = '';
    lastName: string = '';
}

export class Personal {
    address: string = '';
    zip: string = '';
    email: string = '';
}

export class Terms {
    agree: boolean = false;
}

