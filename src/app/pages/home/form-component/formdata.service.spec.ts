import { TestBed, inject } from '@angular/core/testing';

import { FormDataService } from './formdata.service';

describe('FormdataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FormDataService]
    });
  });

  it('should be created', inject([FormDataService], (service: FormDataService) => {
    expect(service).toBeTruthy();
  }));
});
