import { Injectable } from '@angular/core';
import { FormData, General, Personal, Terms, STEPS } from './formData.model';

@Injectable({
  providedIn: 'root'
})
export class FormDataService {

  private formData: FormData = new FormData();
  private isGeneralFormValid: boolean = false;
  private isPersonalFormValid: boolean = false;
  private isTermsFormValid: boolean = false;

  getGeneral(): General {
    const general: General = {
      deal: this.formData.deal,
      firstName: this.formData.firstName,
      lastName: this.formData.lastName,
    };
    return general;
  }

  setGeneral(data: General) {
    this.isGeneralFormValid = true;
    this.formData.firstName = data.firstName;
    this.formData.lastName = data.lastName;
    this.formData.deal = data.deal;
  }

  getPersonal(): Personal {
    const personal: Personal = {
      address: this.formData.address,
      zip: this.formData.zip,
      email: this.formData.email
    };
    return personal;
  }

  setPersonal(data: Personal) {
    this.isPersonalFormValid = true;
    this.formData.address = data.address;
    this.formData.zip = data.zip;
    this.formData.email = data.email;
  }

  getTerms(): Terms {
    const terms: Terms = {
      agree: this.formData.agree,
    };
    return terms;
  }

  setTerms(data: Terms) {
    this.formData.agree = data.agree;
  }

  getFormData(): FormData {
    return this.formData;
  }

  resetFormData(): FormData {
    this.formData.clear();
    this.isGeneralFormValid = this.isPersonalFormValid = this.isTermsFormValid = false;
    return this.formData;
  }

  isFormValid() {
    return this.isGeneralFormValid &&
      this.isPersonalFormValid &&
      this.isTermsFormValid;
  }

  private formSteps = [
    { step: STEPS.general, valid: false },
    { step: STEPS.personal, valid: false },
    { step: STEPS.terms, valid: false }
  ];

  validateStep(step: string) {
    // If the state is found, set the valid field to true 
    var found = false;
    for (var i = 0; i < this.formSteps.length && !found; i++) {
      if (this.formSteps[i].step === step) {
        found = this.formSteps[i].valid = true;
      }
    }
  }

  resetSteps() {
    // Reset all the steps in the Workflow to be invalid
    this.formSteps.forEach(element => {
      element.valid = false;
    });
  }

  getFirstInvalidStep(step: string): string {
    // If all the previous steps are validated, return blank
    // Otherwise, return the first invalid step
    var found = false;
    var valid = true;
    var redirectToStep = '';
    for (var i = 0; i < this.formSteps.length && !found && valid; i++) {
      let item = this.formSteps[i];
      if (item.step === step) {
        found = true;
        redirectToStep = '';
      }
      else {
        valid = item.valid;
        redirectToStep = item.step
      }
    }
    return redirectToStep;
  }
}
