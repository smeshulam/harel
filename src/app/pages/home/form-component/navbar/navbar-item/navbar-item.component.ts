import { Component, OnInit, Output, Input } from '@angular/core';

@Component({
  selector: 'navbar-item',
  templateUrl: './navbar-item.component.html',
  styleUrls: ['../navbar.component.scss']
})
export class NavbarItemComponent implements OnInit {
  @Input() active: Boolean;
  @Input() step: Number;
  @Input() stepName: String;
  @Input() stepTitle: String;

  constructor() { }

  ngOnInit() { }
}
