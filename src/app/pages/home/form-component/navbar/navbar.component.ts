import { Component, OnInit, Input, ContentChildren, forwardRef, QueryList } from '@angular/core';
import { NavbarItemComponent } from './navbar-item/navbar-item.component';

@Component({
  selector: 'navbar-component',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  objectKeys = Object.keys; // for use in expressions
  @Input() public activeStep: number = 0;
  @ContentChildren(forwardRef(() => NavbarItemComponent)) items: QueryList<NavbarItemComponent>;
  public itemsDictionary  = [
    {
      title:'מידע כללי', 
      name: 'general'
    }, 
    {
      title:'פרטי בעל החיסכון',
      name:'personal'
    },
    {
      title:'אישור וסיכום',
      name:'terms'
    }
  ];

  constructor() { }

  ngOnInit() { }

}
